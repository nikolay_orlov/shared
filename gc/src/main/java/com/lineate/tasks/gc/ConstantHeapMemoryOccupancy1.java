package com.lineate.tasks.gc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Supplier;

public class ConstantHeapMemoryOccupancy1 {
	static final Collection<C1> ballastCollection = new ArrayList<>();
	static final BlockingQueue<byte[]> queue = new LinkedBlockingQueue<>();
	static final int testTimeMinutes = 3;
	static final long maxMemory = 4l * 1024 * 1024 * 1024;
	// https://plumbr.io/blog/memory-leaks/how-much-memory-do-i-need-part-2-what-is-shallow-heap
	// https://stackoverflow.com/questions/258120/what-is-the-memory-consumption-of-an-object-in-java
	static final long itemSize = (16 + 8) * 32; // 16 bytes per each reference to class definition + 8 bytes for a reference x 32 times
	static final Supplier<byte[]> producerSupplier = () -> new byte[8 * 1024 * 1024];
	static final Supplier<C1> ballastSupplier = () -> new C1();

	static int memoryOccupancyThresholdPercentage;
	static long startTime;

	public static void main(String[] args) {
		memoryOccupancyThresholdPercentage = args.length > 0 ? Integer.parseInt(args[0]) : 70;
		System.out.println("Memory Occupancy Threshold Percentage: " + memoryOccupancyThresholdPercentage);
		System.out.println("Test Time Minutes: " + testTimeMinutes);
		System.out.println("Item size (b): " + itemSize);
		System.out.println("Max memory (Mb): " + maxMemory / (1024 * 1024));
		setupBallast();
		startTime = System.currentTimeMillis();
		new Thread(new Consumer()).start();
		new Thread(new Producer()).start();
	}
	
	static void setupBallast() {
		while(100.0 * ballastCollection.size() * itemSize / maxMemory < memoryOccupancyThresholdPercentage) {
			ballastCollection.add(ballastSupplier.get());
		}
	}
	
	static class Producer implements Runnable {
		public void run() {
			long produced = 0;
			while(System.currentTimeMillis() - startTime < testTimeMinutes * 60 * 1000) {
				queue.offer(producerSupplier.get());
				produced++;
			}
			System.out.println("Produced items: " + produced);
		}

	}

	static class Consumer implements Runnable {
		public void run() {
			long consumed = 0;
			while(System.currentTimeMillis() - startTime < testTimeMinutes * 60 * 1000) {
					try {
						queue.take();
						consumed++;
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
			}
			System.out.println("Consumed items: " + consumed);
		}
	}

	
	static class C1 {
		C2[] array = new C2[31];
		{
			for (int i = 0; i < 31; i++) {
				array[i] = new C2();
			}
		}
	}
	static class C2 {
	}
}
