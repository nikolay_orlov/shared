package com.lineate.tasks.gc;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Supplier;

public class ConstantHeapMemoryOccupancy2 {
	static final Collection<C1> ballastCollection = new ArrayList<>();
	static final BlockingQueue<byte[]> queue = new LinkedBlockingQueue<>();
	static final int testTimeMinutes = 3;
	static final long maxMemory = 4l * 1024 * 1024 * 1024;
	// https://plumbr.io/blog/memory-leaks/how-much-memory-do-i-need-part-2-what-is-shallow-heap
	// https://stackoverflow.com/questions/258120/what-is-the-memory-consumption-of-an-object-in-java
	static final long itemSize = (16 + 8) * 32; // 16 bytes per each reference to class definition + 8 bytes for a reference x 32 times
	static final Supplier<byte[]> producerSupplier = () -> new byte[8 * 1024 * 1024];
	static final Supplier<C1> ballastSupplier = () -> new C1();

	static int memoryOccupancyThresholdPercentage;
	static long startTime;

	public static void main(String[] args) {
		memoryOccupancyThresholdPercentage = args.length > 0 ? Integer.parseInt(args[0]) : 70;
		System.out.println("Memory Occupancy Threshold Percentage: " + memoryOccupancyThresholdPercentage);
		System.out.println("Test Time Minutes: " + testTimeMinutes);
		System.out.println("Item size (b): " + itemSize);
		System.out.println("Max memory (Mb): " + maxMemory / (1024 * 1024));
		setupBallast();
		startTime = System.currentTimeMillis();
		new Thread(new Consumer()).start();
		new Thread(new Producer()).start();
	}
	
	static void setupBallast() {
		while(100.0 * ballastCollection.size() * itemSize / maxMemory < memoryOccupancyThresholdPercentage) {
			ballastCollection.add(ballastSupplier.get());
		}
	}
	
	static class Producer implements Runnable {
		public void run() {
			long produced = 0;
			while(System.currentTimeMillis() - startTime < testTimeMinutes * 60 * 1000) {
				queue.offer(producerSupplier.get());
				produced++;
			}
			System.out.println("Produced items: " + produced);
		}

	}

	static class Consumer implements Runnable {
		public void run() {
			long consumed = 0;
			while(System.currentTimeMillis() - startTime < testTimeMinutes * 60 * 1000) {
					try {
						queue.take();
						consumed++;
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
			}
			System.out.println("Consumed items: " + consumed);
		}
	}

	
	static class C1 {
		C2 instance = new C2();
	}

	static class C2 {
		C3 instance = new C3();
	}

	static class C3 {
		C4 instance = new C4();
	}

	static class C4 {
		C5 instance = new C5();
	}

	static class C5 {
		C6 instance = new C6();
	}

	static class C6 {
		C7 instance = new C7();
	}

	static class C7 {
		C8 instance = new C8();
	}

	static class C8 {
		C9 instance = new C9();
	}

	static class C9 {
		C10 instance = new C10();
	}

	static class C10 {
		C11 instance = new C11();
	}

	static class C11 {
		C12 instance = new C12();
	}

	static class C12 {
		C13 instance = new C13();
	}

	static class C13 {
		C14 instance = new C14();
	}

	static class C14 {
		C15 instance = new C15();
	}

	static class C15 {
		C16 instance = new C16();
	}

	static class C16 {
		C17 instance = new C17();
	}

	static class C17 {
		C18 instance = new C18();
	}

	static class C18 {
		C19 instance = new C19();
	}

	static class C19 {
		C20 instance = new C20();
	}

	static class C20 {
		C21 instance = new C21();
	}

	static class C21 {
		C22 instance = new C22();
	}

	static class C22 {
		C23 instance = new C23();
	}

	static class C23 {
		C24 instance = new C24();
	}

	static class C24 {
		C25 instance = new C25();
	}

	static class C25 {
		C26 instance = new C26();
	}

	static class C26 {
		C27 instance = new C27();
	}

	static class C27 {
		C28 instance = new C28();
	}

	static class C28 {
		C29 instance = new C29();
	}

	static class C29 {
		C30 instance = new C30();
	}

	static class C30 {
		C31 instance = new C31();
	}

	static class C31 {
		C32 instance = new C32();
	}

	static class C32 {
	}
}
