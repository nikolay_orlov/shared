"C:\Program Files\Java\jdk-14.0.1\bin\java" ^
	-Xmx4096m -XX:+UnlockExperimentalVMOptions -XX:+UseZGC ^
	-Xlog:gc*=debug:zgc_chmo70_alloc.log:time ^
	-XX:StartFlightRecording=disk=true,dumponexit=true,maxsize=1024m,maxage=1d,settings=profile,filename=zgc_chmo70_alloc.jfr ^
	-cp ./../target/gc-app-1-jar-with-dependencies.jar com.lineate.tasks.gc.ConstantHeapMemoryOccupancy2
